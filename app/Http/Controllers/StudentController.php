<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Yajra\Datatables\Datatables;

class StudentController extends Controller
{
    public function Student()
    {
    	return view('student.student');
    }

    public function StoreStudent(Request $request)
    {
    	$validateData = $request->validate([
    		'name' => 'required | min:6',
    		'email' => 'required | unique:students',
    		'phone' => 'required | unique:students'
    	]);

    	$student = new Student();
    	$student->name = $request->name;
    	$student->email = $request->email;
    	$student->phone = $request->phone;
		$insert = $student->save();
		
    	if ($insert) {
    		$notification = array(
    			'message' => 'Successfully Inserted',
    			'alert-type' => 'success'
    		);
    		return redirect()->back()->with($notification);
    	}
    	else{
    		$notification = array(
    			'message' => 'Unsuccessful to insert data',
    			'alert-type' => 'error'
    		);
    		return redirect()->back()->with($notification);
    	}

    }

    public function AllStudent()
    {
    	$student = Student::all();
    	//return response()->json($student);
    	// return view('student.all_student', compact('student'));
        return Datatables::of($student)
                ->addColumn('action', function($student){
                    return '<a href="'.url('/view/student/'.$student->id).'" class="btn btn-sm btn-success">show</a> 
                    <a href="'.url('edit/student/'.$student->id).'" class="btn btn-sm btn-info">Edit</a>
                    <a href="'.url('delete/student/'.$student->id).'" class="btn btn-sm btn-danger" id="delete">Delete</a>';
                })
            ->make(true);
    }

    public function ViewStudent($id)
    {
    	$student = Student::find($id);
    	return view('student.show_student', compact('student'));
    }

    public function DeleteStudent($id)
    {
    	$student = Student::find($id);
    	$delete = $student->delete();
    	if ($delete) {
    		$notification = array(
    			'message' => 'Successfully Delete Data',
    			'alert-type' => 'success'
    		);
    		return redirect()->back()->with($notification);
    	}
    	
    }

    public function EditStudent($id)
    {
    	$student = Student::find($id);
    	return view('student.edit_student', compact('student'));
    }

    public function UpdateStudent(Request $request, $id)
    {
    	$validateData = $request->validate([
    		'name' => 'required | min:6',
    		'email' => 'required',
    		'phone' => 'required'
    	]);

    	$student = Student::find($id);
    	$student->name = $request->name;
    	$student->email = $request->email;
    	$student->phone = $request->phone;
    	$updated = $student->save();

    	if ($updated) {
    		$notification = array(
    			'message' => 'Successfully Updated Data',
    			'alert-type' => 'success'
    		);
    		return redirect()->route('all.student')->with($notification);
    	}
    }
}
