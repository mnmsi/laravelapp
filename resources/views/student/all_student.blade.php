@extends('template.index')

@section('content')
	<div class="container">
        <h2 style="align-content: center;">All Students</h2>
        <table class="table table-bordered" id="table">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Name</th>
                 <th>Email</th>
                 <th>Phone</th>
                 <th>Action</th>
              </tr>
           </thead>
           <tbody>
           	
           </tbody>
        </table>
     </div>
     <script type="text/javascript">
     	var table1 = $('#table').DataTable({
     		processing: true,
     		serverSide: true,
     		ajax: "{{ route('all.student') }}",
     		columns: [
     			{ data: 'id', name: 'id' },
                { data: 'Name', name: 'name' },
                { data: 'Email', name: 'email' },
                { data: 'Phone', name: 'phone' },
                { data: 'action', name: 'Action', orderlable:false, searchable:false }
     		]

     	});
     </script>

@endsection