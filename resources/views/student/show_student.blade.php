@extends('template.index')

@section('content')
	<div class="container">
	    <div class="row">
	      <div class="col-lg-8 col-md-10 mx-auto">

	        <a href="{{ URL::to('/student') }}" class="btn btn-success" >Add Student</a>
	        <a href="{{ route('all.student') }}" class="btn btn-info">All Student</a>
	       <hr>
	       <div>
		      <ol>
		      	<li>Student Id: {{ $student->id }}</li>
	       		<li>Student Name: {{ $student->Name }}</li>
	       		<li>Student Email: {{ $student->Email }}</li>
	       		<li>Student Phone: {{ $student->Phone }}</li>
	       	  </ol>
	       	
	       </div>
	  		
	      </div>
	    </div>
  </div>
@endsection