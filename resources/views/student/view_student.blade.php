@extends('template.index')

@section('content')
	 <div class="container">
	    <div class="row">
	      <div class="col-lg-8 col-md-10 mx-auto">

	        <a href="{{ url('/student') }}" class="btn btn-danger" >Add Student</a>
	        <a href="{{ URL::to('datatables') }}" class="btn btn-info">All Student</a>
	       <hr>
	       	@if($errors->any())
	       		<div class="alert alert-danger">
	       			<ul>
	       				@foreach($errors->all() as $error)
	       					<li> {{ $error }} </li>
	       				@endforeach
	       			</ul>
	       		</div>
	       	@endif
	       <div>
	       	<ol>
	       		<li> Name: {{ $student->Name }}</li>
	       		<li>Email: {{ $student->Email }}</li>
	       		<li>Phone: {{ $student->Phone}}</li>
	       	</ol>
	       </div>
	  		
	      </div>
	    </div>
  </div>


 @endsection