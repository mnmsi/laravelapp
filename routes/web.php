<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/student', 'StudentController@Student');
Route::post('store/student', 'StudentController@StoreStudent')->name('store.student');
Route::get('all/student', 'StudentController@AllStudent')->name('all.student');
Route::get('view/student/{id}', 'StudentController@ViewStudent');
Route::get('delete/student/{id}', 'StudentController@DeleteStudent');
Route::get('edit/student/{id}', 'StudentController@EditStudent');
Route::post('update/student/{id}', 'StudentController@UpdateStudent');
Route::get('/search/student', 'SearchController@Search')->name('search.student');
Route::post('/find/student', 'SearchController@FindStudent')->name('find.student');

//--------------------------------------------------Datatables------------------------------------------//
Route::resource('datatables', 'DatatablesController');



